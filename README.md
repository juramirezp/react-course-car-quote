# Cars Quote
##### ReactJS Course from Udemy

### Link to Demo App:
### https://gifted-khorana-3092a4.netlify.app


---

## Extensions from VS Code

-   JSX HTML
-   Simple React Snippets
-   Reactjs code snippets
-   React/Redux/react-router Snippets
-   ES7 React/Redux/GraphQL/React-Native snippets
-   vscode-styled-components
-   Styled-Snippets

## Shortcuts

### imp

```js
import '' from '';
```

### imr

```js
import React rom 'react';
```

### impt

```js
import PropTypes from 'prop-types'
```

### sfc

```js
const '' = () => {
    return ();
};

export default '';
```

---

##### Link to udemy Course:
##### https://www.udemy.com/course/react-de-principiante-a-experto-creando-mas-de-10-aplicaciones/