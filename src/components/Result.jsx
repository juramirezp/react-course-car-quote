import React, {useState} from 'react'
import styled from '@emotion/styled'
import {TransitionGroup, CSSTransition} from 'react-transition-group'
import PropTypes from 'prop-types';

const TotalContainer = styled.div`
    background-color: rgb(127,224,237);
    text-align: center;
    padding: 5rem;
    border: 1px solid #26c6da;
    margin-top: 1rem;
    position: relative;
`

const TotalText = styled.p`
    color: #00838f;
    padding: 1rem;
    text-transform: uppercase;
    font-weight: bold;
    margin: 0;
`

const Result = ({total}) => {

    if(total === 0) return null;

    return (  

        <TotalContainer>
            <TransitionGroup
                component ="span"
                className="result"
            >
                <CSSTransition
                    classNames="resultado"
                    key={total}
                    timeout={{enter: 500, exit: 500}}
                    >
                    <TotalText><span>El total es: $ {total}</span></TotalText>
                </CSSTransition>
            </TransitionGroup>
        </TotalContainer>
    );
}

Result.propTypes ={
    total: PropTypes.number.isRequired
}
 
export default Result;