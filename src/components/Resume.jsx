import React from 'react'
import styled from '@emotion/styled'
import PropTypes from 'prop-types';

const ResumeContainer = styled.div`
    padding: 1rem;
    text-align: center;
    background-color: #00838f;
    color: white;
    margin-top: 1rem;

    & ul{
        padding: 0;
        text-transform: capitalize;
    }
`;

const Resume = ({data}) => {

    // Extraer datos del state
    const {brand, year, plan} = data;

    if(brand === '' || year === '' || plan === '') return null;

    return ( 
        <ResumeContainer>
            <h2>Resumen de Cotización</h2>
            <ul>
                <li>Marca: {brand}</li>
                <li>Año: {year}</li>
                <li>Plan: {plan}</li>
            </ul>
        </ResumeContainer>
     );
}

Resume.propTypes ={
    data: PropTypes.object.isRequired
}
 
export default Resume;