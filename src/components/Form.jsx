import React, {useState} from 'react';
import styled from '@emotion/styled';
import {getYearDifference, brandCalculate, planCalculate} from '../helper';
import PropTypes from 'prop-types';

const Field = styled.div`
    display: flex;
    margin-bottom: 1rem;
    align-items: center;
`;

const Label = styled.label`
    flex: 0 0 100px;
`;

const Select = styled.select`
    display: block;
    width: 100%;
    padding: 1rem;
    border: 1px solid #e1e1e1;
    -webkit-appearance: none;
`;

const InputRadio = styled.input`
    margin: 0 1rem;
`;

const Button = styled.button`
    background-color: #00838f;
    font-size: 16px;
    width: 100%;
    padding: 1rem;
    color: #fff;
    text-transform: uppercase;
    font-weight: bold;
    border: none;
    transition: all .3s ease;
    margin-top: 2rem;

    &:hover{
        cursor: pointer;
        background-color: #26c6da;
    }
`;

const Error = styled.div`
    background-color: red;
    color: white;
    padding: 1rem;
    width: 100%;
    text-align: center;
    margin-bottom: 2rem;
`;

const Form = ({setResume, setLoading}) => {

    const [data, setData] = useState({
        brand: '',
        year:'',
        plan: ''
    })
    const [error, setError] = useState(false)

    // Extraer valores de state
    const {brand, year, plan} = data;

    // Leer datos del formulario
    const formInformation = e =>{
        setData({
            ...data,
            [e.target.name]: e.target.value
        })
    }

    // Enviar formulario
    const insuranceQuote = e => {
        e.preventDefault();

        if(brand.trim() === '' || year.trim === '' || plan.trim === ''){
            setError(true);
            return;
        }

        setError(false);

        // REGLAS DE NEGOCIO

        // Precio base
        let amount = 2000;

        // Obtener la diferencia de años
        const difference = getYearDifference(year);

        // por cada año -3%
        amount -= ((difference * 3) * amount) / 100;

        // Americano +15%
        // Asiatico +5%
        // Europeo +30%
        amount = brandCalculate(brand) * amount;
        
        
        // basico +20%
        // completo +50%
        amount = parseFloat(planCalculate(plan) * amount).toFixed(2);

        // Mostrar spinner
        setLoading(true);

        setTimeout(() => {
            setLoading(false);
            // Mostrar total
            setResume({
                total: Number(amount),
                data
            })
        }, 1500);
        
    }

    return ( 
        <form
            onSubmit={insuranceQuote}
        >
            {error ? <Error>Todos los campos son obligatorios</Error> : null}
            <Field>
                <Label>Marca</Label>
                <Select
                    name="brand"
                    value={brand}
                    onChange={formInformation}
                >
                    <option value="">-- Seleccione --</option>
                    <option value="americano">Americano</option>
                    <option value="europeo">Europeo</option>
                    <option value="asiatico">Asiatico</option>
                </Select>
            </Field>

            <Field>
                <Label>Año</Label>
                <Select
                    name="year"
                    value={year}
                    onChange={formInformation}
                >
                    <option value="">-- Seleccione --</option>
                    <option value="2021">2021</option>
                    <option value="2020">2020</option>
                    <option value="2019">2019</option>
                    <option value="2018">2018</option>
                    <option value="2017">2017</option>
                    <option value="2016">2016</option>
                    <option value="2015">2015</option>
                    <option value="2014">2014</option>
                    <option value="2013">2013</option>
                    <option value="2012">2012</option>
                </Select>
            </Field>

            <Field>
                <Label>Plan</Label>
                <InputRadio 
                    type="radio" 
                    name="plan"
                    value="basico"
                    checked={plan ==="basico"}
                    onChange={formInformation}
                />Básico

                <InputRadio 
                    type="radio" 
                    name="plan"
                    value="completo"
                    checked={plan ==="completo"}
                    onChange={formInformation}
                />Completo

            </Field>

            <Field>
                <Button type="submit">Cotizar</Button>
            </Field>
        </form>
     );
}

Form.propTypes ={
    setResume: PropTypes.func.isRequired,
    setLoading: PropTypes.func.isRequired
}
 
export default Form;