import React, {useState} from 'react';
import Header from './components/Header'
import styled from '@emotion/styled'
import Form from './components/Form'
import Resume from './components/Resume'
import Result from './components/Result'
import Spinner from './components/Spinner'

const Container = styled.div`
  max-width: 600px;
  margin: 0 auto;
`;

const FormContainer = styled.div`
  background-color: #fff;
  padding: 3rem;
`;

function App() {

  const [resume, setResume] = useState({
    total: 0,
    data: {
      brand:'',
      plan:'',
      year:''
    }
  });
  const [loading, setLoading] = useState(false);

  // Extraer datos del state
  const {data, total} = resume;

  return (
    <Container>
      <Header title='Cotizador de Seguros'>
      </Header>

      <FormContainer>
        <Form
          setResume={setResume}
          setLoading={setLoading}
        ></Form>

        {loading ? <Spinner/> : null}
        
        {!loading ? 
          <Resume
            data={data}
          ></Resume>
        : null}


        {!loading ? 
          <Result
            total = {total}
          ></Result>
        : null}
        

        
      </FormContainer>
    </Container>
  );
}

export default App;
